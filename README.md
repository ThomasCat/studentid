# StudentID

A WearOS app to replicate Indian college IDs

**Tip:** Copy paste the following into terminal for easy data input (where `%s` is a whitespace).

` adb shell input text "id" && adb shell input keyevent 66 && adb shell input text "fname%slname" ` and so on...

<hr />

## [`Download`](https://gitlab.com/tomthecat/studentid/-/raw/master/app/release/app-release.apk)
