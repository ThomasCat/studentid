package com.owais.studentid
import android.R.attr.bitmap
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.Color
import android.os.Bundle
import android.os.Vibrator
import android.support.wearable.activity.WearableActivity
import android.text.Html
import android.util.Base64
import android.util.Log
import android.view.View
import android.widget.*
import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import kotlinx.android.synthetic.main.activity_edit.*
import java.io.ByteArrayOutputStream


class EditActivity : WearableActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit)
        val candidateId = findViewById<EditText>(R.id.CandidateID)
        val name = findViewById<EditText>(R.id.FullNameInput)
        val usn = findViewById<EditText>(R.id.USNInput)
        val department = findViewById<EditText>(R.id.DepartmentInput)
        val college = findViewById<EditText>(R.id.CollegeInput)
        val sharedPrefFile = "data"
        val sharedPreferences: SharedPreferences = this.getSharedPreferences(sharedPrefFile, Context.MODE_PRIVATE)
        val bloodGroups = arrayOf(Html.fromHtml("<center><i>Blood Group</i></center>"), "A+", "O+", "B+", "AB+", "A-", "O-", "B-", "AB-")
        val semesters = arrayOf(Html.fromHtml("<center><i>Semester</i></center>"), "1st Semester", "2nd Semester", "3rd Semester", "4th Semester", "5th Semester", "6th Semester", "7th Semester", "8th Semester")
        candidateId.setText(sharedPreferences.getString("id_key", ""))
        name.setText(sharedPreferences.getString("name_key", ""))
        usn.setText(sharedPreferences.getString("usn_key", ""))
        department.setText(sharedPreferences.getString("department_key", ""))
        college.setText(sharedPreferences.getString("college_key", ""))
        val editor:SharedPreferences.Editor =  sharedPreferences.edit()
        /////////////////////////////////
        val bloodGroupAdapter = ArrayAdapter(
            this, // Context
            android.R.layout.simple_spinner_dropdown_item, // Layout
            bloodGroups // Array
        )
        BloodGroupInput.adapter = bloodGroupAdapter
        val bloodGroupValue = sharedPreferences.getString("blood_group_key", "")
        if (bloodGroupValue == "") {
            BloodGroupInput.setSelection(0, false)
        } else {
            BloodGroupInput.setSelection(bloodGroupAdapter.getPosition(bloodGroupValue))
        }
        BloodGroupInput.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent:AdapterView<*>, view: View, position: Int, id: Long) {
                editor.putString("blood_group_key", parent.getItemAtPosition(position).toString())
            }
            override fun onNothingSelected(parent: AdapterView<*>){
                // Another interface callback
            }
        }
        ///////////////////////////////////////
        val semesterAdapter = ArrayAdapter(
            this, // Context
            android.R.layout.simple_spinner_dropdown_item, // Layout
            semesters // Array
        )
        SemesterInput.adapter = semesterAdapter
        val semesterValue = sharedPreferences.getString("semester_key", "")
        if (semesterValue == "") {
            SemesterInput.setSelection(0, false)
        } else {
            SemesterInput.setSelection(semesterAdapter.getPosition(semesterValue))
        }
        SemesterInput.setSelection(semesterAdapter.getPosition(sharedPreferences.getString("semester_key", "")))
        SemesterInput.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent:AdapterView<*>, view: View, position: Int, id: Long) {
                editor.putString("semester_key", parent.getItemAtPosition(position).toString())
            }
            override fun onNothingSelected(parent: AdapterView<*>){
                // Another interface callback
            }
        }
        ////////////////////////
        SaveButton.setOnClickListener (View.OnClickListener{
            if (name.text.toString().trim().isEmpty()) {
                Toast.makeText(applicationContext, "Please enter name", Toast.LENGTH_SHORT).show()
            } else if (candidateId.text.toString().trim().isEmpty()) {
                Toast.makeText(applicationContext, "Please enter candidate ID", Toast.LENGTH_SHORT).show()
            } else if (usn.text.toString().trim().isEmpty()) {
                Toast.makeText(applicationContext, "Please enter USN", Toast.LENGTH_SHORT).show()
            } else if (department.text.toString().trim().isEmpty()) {
                Toast.makeText(applicationContext, "Please enter department", Toast.LENGTH_SHORT).show()
            } else if (college.text.toString().trim().isEmpty()) {
                Toast.makeText(applicationContext, "Please enter college", Toast.LENGTH_SHORT).show()
            } else if (BloodGroupInput.selectedItemPosition==0) {
                Toast.makeText(applicationContext, "Please select blood group", Toast.LENGTH_SHORT).show()
            } else if (SemesterInput.selectedItemPosition==0) {
                Toast.makeText(applicationContext, "Please select semester", Toast.LENGTH_SHORT).show()
            }else{
                // generate and put focused and bright barcode as base64 in SharedPrefs
                val vibratorService = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
                vibratorService.vibrate(100)
                val writer = MultiFormatWriter()
                val bm = writer.encode(candidateId.text.toString(), BarcodeFormat.CODE_128, 950, 360) //canvas
                val normalImageBitmap = Bitmap.createBitmap(950, 359, Bitmap.Config.ARGB_8888) // barcode
                val focusedImageBitmap = Bitmap.createBitmap(950, 359, Bitmap.Config.ARGB_8888) // barcode
                for (x in 0..900) {
                    for (y in 0..358) {
                        normalImageBitmap.setPixel(x, y, if (bm[x, y]) Color.BLACK else Color.WHITE)
                        focusedImageBitmap.setPixel(x, y, if (bm[x, y]) Color.BLACK else Color.GRAY)
                    }
                }
                val normalOutputStream = ByteArrayOutputStream()
                normalImageBitmap.compress(Bitmap.CompressFormat.WEBP, 0, normalOutputStream)
                var normalBarcodeBase64 = Base64.encodeToString(normalOutputStream.toByteArray(), Base64.DEFAULT)

                val focusedOutputStream = ByteArrayOutputStream()
                focusedImageBitmap.compress(Bitmap.CompressFormat.WEBP, 0, focusedOutputStream)
                var focusedBarcodeBase64 = Base64.encodeToString(focusedOutputStream.toByteArray(), Base64.DEFAULT)

                editor.putString("normal_barcode", normalBarcodeBase64)
                editor.putString("focused_barcode", focusedBarcodeBase64)
                // end barcode insertion

                editor.putString("id_key", candidateId.text.toString())
                editor.putString("name_key", name.text.toString())
                editor.putString("usn_key", usn.text.toString())
                editor.putString("department_key", department.text.toString())
                editor.putString("college_key", college.text.toString())
                editor.commit()

                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
                finish()
                Toast.makeText(applicationContext, "Data saved!", Toast.LENGTH_SHORT).show()
            }
        })
        CancelButton.setOnClickListener (View.OnClickListener{
            val sharedPrefFile = "data"
            val sharedPreferences: SharedPreferences = this.getSharedPreferences(sharedPrefFile, Context.MODE_PRIVATE)
            if (sharedPreferences.getString("id_key", "") == "" || sharedPreferences.getString("name_key", "") == "" || sharedPreferences.getString("usn_key", "") == "" || sharedPreferences.getString("department_key", "") == "" || sharedPreferences.getString("college_key", "") == ""){
                finish()
            } else {
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
                finish()
                val vibratorService = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
                vibratorService.vibrate(50)
            }
        })
    }
}

