package com.owais.studentid

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Vibrator
import android.support.wearable.activity.WearableActivity
import android.widget.TextView
import android.widget.Toast
import androidx.wear.activity.ConfirmationActivity
import com.google.android.wearable.intent.RemoteIntent
import kotlinx.android.synthetic.main.activity_about.*


class AboutActivity : WearableActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)
        val urlLink = findViewById<TextView>(R.id.sourceCode)
        urlLink.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW)
                .addCategory(Intent.CATEGORY_BROWSABLE)
                .setData(Uri.parse("https://www.gitlab.com/ThomasCat/studentid"))
            RemoteIntent.startRemoteActivity(this, intent, null)

            Toast.makeText(applicationContext,"URL opened on \npaired phone", Toast.LENGTH_SHORT).show()
        }
        BackButton.setOnClickListener {
            finish()
            val vibratorService = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
            vibratorService.vibrate(100)
        }
    }
}
