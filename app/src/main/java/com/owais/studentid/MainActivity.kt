package com.owais.studentid
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.res.ColorStateList
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Vibrator
import android.support.wearable.activity.WearableActivity
import android.util.Base64
import android.view.Gravity
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.wear.widget.BoxInsetLayout
import kotlinx.android.synthetic.main.activity_main.*
import java.text.SimpleDateFormat
import java.util.*


class MainActivity : WearableActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val sharedPrefFile = "data"
        val sharedPreferences: SharedPreferences = this.getSharedPreferences(sharedPrefFile, Context.MODE_PRIVATE)
        val nameData = sharedPreferences.getString("name_key", "Default")
        val departmentData = sharedPreferences.getString("department_key","Default")
        val candidateIdData = sharedPreferences.getString("id_key","Default")
        val bloodGroupData = sharedPreferences.getString("blood_group_key","Default")
        val semesterData = sharedPreferences.getString("semester_key","Default")
        val collegeData = sharedPreferences.getString("college_key","Default")
        val usnData = sharedPreferences.getString("usn_key","Default")
        val name = findViewById<TextView>(R.id.FullName)
        val department = findViewById<TextView>(R.id.Department)
        val candidateId = findViewById<TextView>(R.id.CandidateID)
        val college = findViewById<TextView>(R.id.College)
        val year = findViewById<TextView>(R.id.Year)
        val semester = findViewById<TextView>(R.id.Semester)
        val bloodGroup = findViewById<TextView>(R.id.BloodGroup)
        val usn = findViewById<TextView>(R.id.USN)
        val afterBarcode= findViewById<LinearLayout>(R.id.AfterBarcode)
        val beforeBarcode= findViewById<LinearLayout>(R.id.BeforeBarcode)
        val barcode = findViewById<ImageView>(R.id.Barcode)
        val barcodeContainer = findViewById<LinearLayout>(R.id.BarcodeContainer)
        val editButton = findViewById<ImageView>(R.id.EditButton)
        val boxInsetLayout = findViewById<BoxInsetLayout>(R.id.BoxInsetLayout)
        fun getBarcode (mode: String?) {
            if (mode == "normal") {
                val decodedBytes: ByteArray = Base64.decode(sharedPreferences.getString("normal_barcode", "")
                    ?.substring(sharedPreferences.getString("normal_barcode", "")!!.indexOf(",") + 1), Base64.DEFAULT)
                val image = BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.size)
                barcode.setImageBitmap(image)
            }
            if (mode == "focused") {
                val decodedBytes: ByteArray = Base64.decode(sharedPreferences.getString("focused_barcode", "")
                    ?.substring(sharedPreferences.getString("focused_barcode", "")!!.indexOf(",") + 1), Base64.DEFAULT)
                val image = BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.size)
                barcode.setImageBitmap(image)
            }
        }
        fun academicYear(): String{
            val monthData = SimpleDateFormat("M")
            val yearData = SimpleDateFormat("YYYY")
            val currentMonth = monthData.format(Date()).toInt()
            val currentYear = yearData.format(Date()).toInt()
            var academicYear=""
            if ( currentMonth >= 6 ) {
                val year1 = currentYear.toString()
                val year2 = currentYear+1
                val year2str = year2.toString()
                academicYear = "$year1—$year2str"
            }else if( currentMonth < 6 ) {
                val year1 = currentYear-1
                val year1str = year1.toString()
                val year2 = currentYear.toString()
                academicYear = "$year1str—$year2"
            }
            return academicYear
        }
        // Barcode initialized and focusing mode
        getBarcode("normal")
        var focusStatus = 0
        barcode.setOnClickListener {
            // enable focus for 10 seconds
            val handler = Handler()
            handler.postDelayed(Runnable {
                boxInsetLayout.setBackgroundColor(Color.WHITE)
                afterBarcode.setBackgroundColor(Color.WHITE)// = ColorStateList.valueOf(Color.parseColor("#FFFFFF"))
                beforeBarcode.setBackgroundColor(Color.WHITE)// = ColorStateList.valueOf(Color.parseColor("#FFFFFF"))
                barcodeContainer.foregroundTintList = ColorStateList.valueOf(Color.parseColor("#AA88AA"))
                editButton.backgroundTintList = ColorStateList.valueOf(Color.parseColor("#1766A6"))
                editButton.imageTintList = ColorStateList.valueOf(Color.parseColor("#FFFFFF"))
                bloodGroup.compoundDrawableTintList = ColorStateList.valueOf(Color.parseColor("#FF0000"))
                getBarcode("normal")
                val normalToast=Toast.makeText(applicationContext,"Normal", Toast.LENGTH_SHORT)
                normalToast.setGravity(Gravity.BOTTOM, 0, 0)
                normalToast.show()
            }, 10000)
            boxInsetLayout.setBackgroundColor(Color.GRAY)
            afterBarcode.setBackgroundColor(Color.BLACK)// = ColorStateList.valueOf(Color.parseColor("#000000"))
            beforeBarcode.setBackgroundColor(Color.BLACK)// = ColorStateList.valueOf(Color.parseColor("#000000"))
            barcodeContainer.foregroundTintList = ColorStateList.valueOf(Color.parseColor("#000000"))
            editButton.backgroundTintList = ColorStateList.valueOf(Color.parseColor("#000000"))
            editButton.imageTintList = ColorStateList.valueOf(Color.parseColor("#000000"))
            bloodGroup.compoundDrawableTintList = ColorStateList.valueOf(Color.parseColor("#000000"))
            getBarcode("focused")
            val focusToast=Toast.makeText(applicationContext,"Focused", Toast.LENGTH_SHORT)
            focusToast.setGravity(Gravity.BOTTOM, 0, 0)
            focusToast.show()
        }

        if(candidateIdData.equals("Default")||nameData.equals("Default")||departmentData.equals("Default")|| collegeData.equals("Default")||usnData.equals("Default")){
            val intent = Intent(this, EditActivity::class.java)
            startActivity(intent)
            finish()
        }else{
            candidateId.setText(candidateIdData).toString()
            name.setText(nameData).toString()
            department.setText(departmentData).toString()
            college.setText(collegeData).toString()
            year.setText(academicYear()).toString()
            bloodGroup.setText(bloodGroupData).toString()
            semester.setText(semesterData).toString()
            usn.setText(usnData).toString()
        }
        EditButton.setOnClickListener {
            val intent = Intent(this, EditActivity::class.java)
            startActivity(intent)
            finish()
            val vibratorService = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
            vibratorService.vibrate(100)
        }
        AboutButton.setOnClickListener {
            val intent = Intent(this, AboutActivity::class.java)
            startActivity(intent)
            val vibratorService = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
            vibratorService.vibrate(50)
        }
    }
}



